import io
import os
import sys
import glob
import errno

path = '/home/xavient/Desktop/GoogleApi/wav-files/*.wav'   

def transcribe_file(speech_file):
    """Transcribe the given audio file."""
    from google.cloud import speech
    from google.cloud.speech import enums
    from google.cloud.speech import types
    client = speech.SpeechClient()

    with io.open(speech_file, 'rb') as audio_file:
        content = audio_file.read()

    audio = types.RecognitionAudio(content=content)
    config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.FLAC,
            sample_rate_hertz=44100,
            audio_channel_count=2,
            language_code='en-US')

    response = client.recognize(config, audio)
    # Each result is for a consecutive portion of the audio. Iterate through
    # them to get the transcripts for the entire audio file.
    for result in response.results:
        # The first alternative is the most likely one for this portion.
        print(u'Trans {}'.format(result.alternatives[0].transcript))
    
    fo = open("VOICE-TEXT.txt", "a+")
    fo.write(format(result.alternatives[0].transcript))
    fo.close()



if __name__ == "__main__":
    import sys
    if len(sys.argv) != 1:
        print ("Invalid argument. Exactly one wave file location required as argument")
    else:
        files = glob.glob(path)   
        for name in files: # 'file' is a builtin type, 'name' is a less-ambiguous variable name.
            transcribe_file(name)
